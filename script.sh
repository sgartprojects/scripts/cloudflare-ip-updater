## Config - Edit this to fit your setup
IP_LIST="https://www.cloudflare.com/ips-v4"
TARGET_FILE="/home/npm/data/nginx/custom/cf-real-ip.conf"
TEMP_FILE="/tmp/cf-ips"

## Script - Do not edit if you don't know what you're doing
echo "Fetching latest list..."
wget -q "${IP_LIST}" -O -> "${TEMP_FILE}"

echo "Clearing old config..."
echo "" > "${TARGET_FILE}"

echo "Generating new conf from list..."
while read i
do
    echo "set_real_ip_from ${i};" >> "${TARGET_FILE}"
done < "${TEMP_FILE}"

echo "Adding final config..."
echo "real_ip_header CF-Connecting-IP;" >> "${TARGET_FILE}"

echo "Cleaning up..."
rm -f "${TEMP_FILE}"